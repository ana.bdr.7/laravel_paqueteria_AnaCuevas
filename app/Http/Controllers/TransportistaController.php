<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Transportista;

class TransportistaController extends Controller
{
    public function index(){

        $transportistas = Transportista::all();

        return view('index',['transportistas' => $transportistas]);
    }

    public function show($slug){

        $transportista = DB::table('transportistas')->where('slug',$slug)->first();

        return view('transportista.show',['transportista' => $transportista]);
    }
}
