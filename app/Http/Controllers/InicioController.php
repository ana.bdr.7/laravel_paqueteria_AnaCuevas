<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InicioController extends Controller
{
    public function inicio(){

        $transportistas = DB::table('transportistas')->get();
     
        return view('index', ['transportistas' => $transportistas]);
    }
}
