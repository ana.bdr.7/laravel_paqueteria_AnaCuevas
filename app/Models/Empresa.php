<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\TransportistaController;

class Empresa extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function transportistas(){
        return $this->belongsToMany(Transportista::class, 'id_transportista','transportistas_empresas');
    }
}
