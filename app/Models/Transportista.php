<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\PaqueteController;

class Transportista extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function empresas(){
        return $this->belongsToMany(Empresa::class, 'id_empresa','transportistas_empresas');
    }

    public function paquetes(){
        return $this->hasMany(Paquete::class);
    }
}
