<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\TransportistaController;

class Paquete extends Model
{
    protected $guarded = [];
    use HasFactory;

    function transportistas(){
        return $this->belongsTo(Transportista::class);
    }
}
