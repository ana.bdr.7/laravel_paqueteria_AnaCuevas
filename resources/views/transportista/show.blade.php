@extends('layouts.master') 
 
@section('titulo')
  
@endsection 
 
@section('contenido')
@if(session('mensaje'))
<div class="alert alert-info">
    {{ session('mensaje') }}
</div>
@endif
<div class="row"> 
    <div class="col-sm-3"> 
        <img class="imagen" src="{{asset('/assets/imagenes/transportistas' .$transportista->imagen)}}">
    </div> 
    <div class="col-sm-9"> 
    
       <p><strong>{{$transportista->nombre}}</strong></p>
       
       <p>{{$transportista->fechaPermisoConducir}}</p>
           
     
       
       
    </div> 
</div>  
@endsection 