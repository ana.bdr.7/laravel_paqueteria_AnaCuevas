@extends('layouts.master') 
 
@section('titulo')

@endsection 
 
@section('contenido')
<div class="container">
    <div class="row">
 
        @foreach($transportistas as $clave => $transportista)
            <div class="col-sm-12 col-md-4">        
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('assets\imagenes\transportistas\' .$transportista->imagen)}}"/>
                        <div class="card-body">
                            <h5 class="card-title">{{$transportista->nombre}}</h5>
                            <p class="card-text"> paquetes pendientes de entrega</p>
                            <a href="{{ route('transportista.show', $transportista->slug ) }}"> detalle</a>
                        </div>
                 </div>   
            </div>
        @endforeach
     </div>  
@endsection